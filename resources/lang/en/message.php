<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'account' => [
        'listed_success' => 'Successfully display accounts',
        'created_success' => 'Successfully created account',
        'showed_success' => 'Successfully display account',
        'updated_success' => 'Successfully update account',
        'updated_fail' => 'Failed to update account',
        'deleted_success' => 'Successfully update account',
        'deleted_fail' => 'Failed to delete account',
        'deleted_fail_lock' => 'Failed to delete the account, because the account is locked',
    ],

    'account_category' => [
        'listed_success' => 'Successfully display account categories',
        'created_success' => 'Successfully created account category',
        'showed_success' => 'Successfully display account category',
        'updated_success' => 'Successfully update account category',
        'updated_fail' => 'Failed to update account category',
        'deleted_success' => 'Successfully delete account category',
        'deleted_fail' => 'Failed to delete account category',
    ],

    'business' => [
        'listed_success' => 'Successfully display businesses',
        'created_success' => 'Successfully created business',
        'showed_success' => 'Successfully display business',
        'updated_success' => 'Successfully update business',
        'updated_fail' => 'Failed to update business',
        'deleted_success' => 'Successfully update business',
        'deleted_fail' => 'Failed to delete business',
    ]

];
