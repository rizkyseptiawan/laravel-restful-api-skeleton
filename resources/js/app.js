window.Vue = require('vue').default;

import AppMain from './Main.vue';
import vuetify from './vuetify';
import router from './router';
import store from './stores'
import i18n from "./plugins/i18n";
import api from './api'
import { mapActions, mapState, mapGetters } from 'vuex'
import VueToast from 'vue-toast-notification';
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';

import CountryFlag from 'vue-country-flag'
Vue.component('country-flag', CountryFlag)

// Create global variable
Vue.prototype.$api = api
Vue.prototype.ACCESS = Vue.observable({
    PERMISSIONS: [],
    ROLES: [],
  });

Vue.directive('can', function (el, binding) {
    return this.ACCESS.PERMISSIONS.indexOf(binding) !== -1;
})

Vue.directive('role', function (el, binding) {
    return this.ACCESS.ROLES.indexOf(binding) !== -1;
})

Vue.use(VueToast, {
    duration: 2000,
    position: 'top-right'
})

let globalData = new Vue({
    data: { $successMessage: '', $errorMessage: '' }
});

Vue.mixin({
    methods: {
        hasRole(role) {

            return this.ACCESS.ROLES.indexOf(role) !== -1;
        },
        hasPermission(permission){
            return this.ACCESS.PERMISSIONS.indexOf(permission) !== -1;
        }
    },
    computed: {
        $successMessage: {
          get: function () { return globalData.$data.$successMessage },
          set: function (newVal) { globalData.$data.$successMessage = newVal; }
        },
        $errorMessage: {
          get: function () { return globalData.$data.$errorMessage },
          set: function (newVal) { globalData.$data.$errorMessage = newVal; }
        },
    },
    watch: {
        '$successMessage'(val) {
            if(val){
                this.$toast.success(val)
                this.$successMessage = ''
            }
        },
        '$errorMessage'(val) {
            if(val){
                this.$toast.error(val)
                this.$errorMessage = ''
            }
        },
    },
})

new Vue({
    vuetify,
    router,
    store,
    i18n,
    el: '#app',
    computed: {
        ...mapGetters(['isAuth']),
        ...mapState('auth', {
            authenticatedUser: state => state.authenticated
        })
    },
    methods: {
        ...mapActions('auth', ['getAuthenticatedUser']),
        ...mapActions(['setToken'])
    },
    mounted () {
        const that = this
        window.addEventListener('localstorage_token_changed', (event) => {
            that.setToken(event.detail.modifiedToken)
        });
    },
    async created () {
        if(this.isAuth){
            await this.getAuthenticatedUser()
            console.log('ini dari app.js', this.ACCESS)
        }
    },
    render: h => h(AppMain)
});
