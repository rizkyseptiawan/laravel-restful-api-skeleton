import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// Import modules
import auth from './auth'

export default new Vuex.Store({
    modules: {
        auth
    },
    state: {
        token : localStorage.getItem('token') != null ? localStorage.getItem('token') : null ,
    },
    getters: {
        isAuth: state => {
            return state.token != "null" && state.token != null
        },
    },
    mutations: {
        SET_TOKEN(state, payload) {
            state.token = payload;
        }
    },
    actions: {
        setToken({commit}, payload) {
            commit('SET_TOKEN', payload);
        }
    }
})
