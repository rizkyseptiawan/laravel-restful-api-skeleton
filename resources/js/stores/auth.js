import Vue from 'vue'

const state = {
    authenticated : '',
    processing: false
}
const mutations = {
    SET_AUTHENTICATED(state, payload){
        state.authenticated = payload
    },
    SET_PROCESSING(state, payload){
        state.processing = payload
    }

}
const actions = {
    getAuthenticatedUser({ commit }){
        commit('SET_PROCESSING', true)
        return new Promise(
            (resolve, reject) => {
                Vue.prototype.$api.get(
                    '/auth/authenticated-user'
                ).then(
                    (res) => {
                        const responseData = res.data
                        commit('SET_AUTHENTICATED', responseData.data.personal)
                        Vue.prototype.ACCESS.ROLES = responseData.data.roles
                        Vue.prototype.ACCESS.PERMISSIONS = responseData.data.all_permissions
                        resolve(res)
                    },
                    (err) => {
                        const responseData = err.response.data
                        reject(responseData)
                    }
                )
            }
        )
    }
}
const getters = {}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
