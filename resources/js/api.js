import axios from 'axios';
import i18n from './plugins/i18n'

const $axios = axios.create({
    baseURL: '/api',
    headers: {
        'Content-Type': 'application/json'
    }
});
// Set param config before request to 
$axios.interceptors.request.use (
    function (config) {
        const token = localStorage.getItem('token') != null ? localStorage.getItem('token') : null
        if (token) config.headers.Authorization = `Bearer ${token}`;
        config.headers["X-LOCALIZATION"] = localStorage.getItem('locale_lang') != null ? localStorage.getItem('locale_lang') : i18n.locale
        return config;
    },
    function (error) {
        return Promise.reject (error);
    }
);

$axios.interceptors.response.use (
    function (response) {
        return response
    },
    function (error) {
        if(error.response.status === 401 && error.response.data.code == 1401){
            localStorage.removeItem('token')
            window.location = '/auth/login';
        }
        if(error.response.status === 403){
            window.location = '/';
        }
        return Promise.reject (error.response);
    }
);

export default $axios;
