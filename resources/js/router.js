import Vue from 'vue'
import VueRouter from 'vue-router';

import AuthLayout from './layouts/Auth.vue';
import PanelLayout from './layouts/Panel.vue';

import LoginPage from './pages/Auth/Login.vue';
import ForgotPage from './pages/Auth/Forgot.vue';
import ResetPage from './pages/Auth/Reset.vue';

import Home from './pages/Home.vue';
import About from './pages/About.vue';

// Component
import PageNotFound from './components/PageNotFound';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/auth',
            name: 'AuthLayout',
            component: AuthLayout,
            children: [
                {
                    path: '',
                    name: 'auth',
                    redirect: { name: 'auth.login' }
                },
                {
                    path: 'login',
                    name: 'auth.login',
                    component: LoginPage,
                    meta: { title: 'Login'},
                },
                {
                    path: 'forgot',
                    name: 'auth.forgot',
                    component: ForgotPage,
                    meta: { title: 'Forgot Password'},
                },
                {
                    path: 'reset',
                    name: 'auth.reset',
                    component: ResetPage,
                    meta: { title: 'Reset Password'},
                },
            ]
        },
        {
            path: '/',
            name: 'PanelLayout',
            component: PanelLayout,
            children: [
                {
                    path: '',
                    name: 'home',
                    component: Home
                },
                {
                    path: '/about',
                    name: 'about',
                    component: About
                }
            ]
        },
        // Not FOund ROuter
        { path: "*", component: PageNotFound }
    ]
});

export default router;