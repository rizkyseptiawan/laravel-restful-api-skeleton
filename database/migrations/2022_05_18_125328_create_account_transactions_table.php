<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('business_location_id');
            $table->string('transaction_number')->unique();
            $table->unsignedDecimal('balance', 15, 2);
            $table->enum('balance_type', ['debit','credit']);
            $table->enum('transaction_type',['deposit','transfer','withdraw']);
            $table->enum('account_transaction_origin_type',['internal','external'])->nullable();
            $table->string('account_transaction_origin_resource')->nullable();
            $table->string('account_transaction_description')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_transactions');
    }
};
