<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings =[
            [
                'key' => 'business_name',
                'value' => 'LabdaCaraka',
                'type' => 'general',
            ]
        ];
        foreach ($settings as $setting) {
            Setting::create($setting);
        }
    }
}
