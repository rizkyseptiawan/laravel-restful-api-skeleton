<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\AccountCategory;

class AccountCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accountCategories = [
            [
                'account_category_name' => 'Cash & Bank',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Accounts Receivable (A/R)',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Inventory',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Current Assets',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Fixed Assets',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Amortization Accummulation',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Depreciation Accummulation',
                'account_category_type' => 'asset'
            ],
            [
                'account_category_name' => 'Accounts Payable (A/P)',
                'account_category_type' => 'liability'
            ],
            [
                'account_category_name' => 'Current Liabilities',
                'account_category_type' => 'liability'
            ],
            [
                'account_category_name' => 'Long-term Debt',
                'account_category_type' => 'liability'
            ],
            [
                'account_category_name' => 'Equity',
                'account_category_type' => 'equity'
            ],
            [
                'account_category_name' => 'Sales Income',
                'account_category_type' => 'revenue'
            ],
            [
                'account_category_name' => 'Investment Income',
                'account_category_type' => 'revenue'
            ],
            [
                'account_category_name' => 'Other Income',
                'account_category_type' => 'revenue'
            ],
            [
                'account_category_name' => 'Cost of Sales',
                'account_category_type' => 'expense'
            ],
            [
                'account_category_name' => 'Other Expenses',
                'account_category_type' => 'expense'
            ],
            [
                'account_category_name' => 'Amortization Expense',
                'account_category_type' => 'expense'
            ],
            [
                'account_category_name' => 'Depreciation Expense',
                'account_category_type' => 'expense'
            ],

        ];

        foreach ($accountCategories as $category) {
            AccountCategory::create($category);
        }
    }
}
