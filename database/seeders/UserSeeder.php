<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'data' => [
                    'firstname' => 'Admin',
                    'username' => 'admin',
                    'email' => 'admin@labdacaraka.com',
                    'phone' => '628123456789',
                    'password' => bcrypt('labdacaraka'),
                    'email_verified_at' => now(),
                    'phone_verified_at' => now()
                ],
                'roles' => ['Admin']
            ],
            [
                'data' => [
                    'firstname' => 'Staff',
                    'username' => 'staff',
                    'email' => 'staff@labdacaraka.com',
                    'phone' => '628123456780',
                    'password' => bcrypt('labdacaraka'),
                    'email_verified_at' => now(),
                    'phone_verified_at' => now()
                ],
                'roles' => ['Staff']
            ],
            [
                'data' => [
                    'firstname' => 'Customer',
                    'username' => 'customer',
                    'email' => 'customer@labdacaraka.com',
                    'phone' => '628123456781',
                    'password' => bcrypt('labdacaraka'),
                    'email_verified_at' => now(),
                    'phone_verified_at' => now()
                ],
                'roles' => ['Customer']
            ]
        ];
        foreach ($users as $user) {
            $createdUser = User::create($user['data']);
            $createdUser->assignRole($user['roles']);
        }
    }
}
