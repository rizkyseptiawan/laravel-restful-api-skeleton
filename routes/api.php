<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\AccountController;
use App\Http\Controllers\Api\AccountCategoryController;
use App\Http\Controllers\Api\BusinessController;
use App\Models\Setting;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function () {
    $result = [
        'name' => 'Api Webservice of LabdaCaraka',
        'version' => '1.0.0',
    ];
    return response()->json($result, 200);
});
Route::get('/unauthorized', [AuthController::class, 'unauthorized'])->name('unauthorized');

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login',[AuthController::class, 'login']);
    Route::post('/signup',[AuthController::class, 'register']);
    Route::post('/forgot-password',[AuthController::class, 'forgotPassword']);
    Route::post('/reset-password',[AuthController::class, 'resetPassword']);
});

// Authenticated route 
Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('/logout',[AuthController::class, 'logout']);
        Route::get('/authenticated-user',[AuthController::class, 'authenticatedUser']);
    });

    // Account Route
    Route::group(['prefix' => 'accounts', 'as' => 'accounts.' ], function () {
        Route::get('/',[AccountController::class, 'index']);
        Route::get('/{id}',[AccountController::class, 'show']);
        Route::post('/',[AccountController::class, 'store']);
        Route::post('/{id}',[AccountController::class, 'update']);
        Route::delete('/{id}',[AccountController::class, 'destroy']);
    });

    // Account Category Route
    Route::group(['prefix' => 'account-categories', 'as' => 'account_categories.' ], function () {
        Route::get('/',[AccountCategoryController::class, 'index']);
        Route::get('/{id}',[AccountCategoryController::class, 'show']);
        Route::post('/',[AccountCategoryController::class, 'store']);
        Route::post('/{id}',[AccountCategoryController::class, 'update']);
        Route::delete('/{id}',[AccountCategoryController::class, 'destroy']);
    });

    Route::group(['prefix' => 'businesses', 'as' => 'businesses.' ], function () {
        Route::get('/',[BusinessController::class, 'index']);
        Route::get('/{id}',[BusinessController::class, 'show']);
        Route::post('/',[BusinessController::class, 'store']);
        Route::post('/{id}',[BusinessController::class, 'update']);
        Route::delete('/{id}',[BusinessController::class, 'destroy']);
    });
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
