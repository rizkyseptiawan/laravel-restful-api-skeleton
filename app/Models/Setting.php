<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    /**
     * Get setting val by key
     *
     * @param [type] $query
     * @param [type] $value
     * @return void
     */
    public function scopeGetVal($query, $value)
    {
        $data = $query->where('key', $value)->first();
        if(is_null($data)){
            return null;
        }
        return $data->value;
    }

    /**
     * Get all settings by type of settings
     *
     * @param [type] $query
     * @param [type] $value
     * @return void
     */
    public function scopeGetByType($query,$value)
    {
        $dataSetting = $query->where('type', $value)->get();
        if(!$dataSetting){
            return null;
        }
        $settings = [];
        foreach ($dataSetting as $setting) {
            $settings[$setting->key] = $setting->value;
        }
        return $settings;
    }
}
