<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountCategory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $appends = ['account_category_number_prefix'];

    public function getAccountCategoryNumberPrefixAttribute()
    {
        $accountCategoryTypeCodes = [
            'asset' => '1-',
            'liability' =>'2-',
            'equity' => '3-',
            'revenue' => '4-',
            'expense' => '5-'
        ];

        $accountCategoryIds = AccountCategory::where('account_category_type', $this->account_category_type)
                                            ->orderBy('id','asc')
                                            ->pluck('id')->toArray();
        $accountCategoryIndex = array_search($this->id, $accountCategoryIds);
        return $accountCategoryTypeCodes[$this->account_category_type].$accountCategoryIndex;
    }
}
