<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use HasFactory;
    
    protected $guarded = ['id'];

    /**
     * Get the main_business_location that owns the Business
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function main_business_location()
    {
        return $this->hasOne(BusinessLocation::class, 'business_id');
    }
}
