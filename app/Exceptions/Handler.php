<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        // $this->reportable(function (Throwable $e) {
        // });
        $this->renderable(function (Throwable $e) {
            if(request()->is('api*')){
                $errorCode = 500;
                if ($e instanceof \Illuminate\Auth\AuthenticationException){
                    $errorCode = 401;
                }
                if(method_exists($e, 'getStatusCode')){
                    $errorCode = $e->getStatusCode();
                }elseif(isset($e->status)){
                    $errorCode = $e->status;
                }
                $errorData = method_exists($e, 'errors') ? $e->errors() : null;
                $response = [
                    'code' => (1000 + $errorCode),
                    'status'  => false,
                    'message' => $e->getMessage()
                ];
                if(!is_null($errorData)){
                    $response['errors'] = $errorData;
                }
                return response()->json($response, $errorCode);
            }
        });
    }
}
