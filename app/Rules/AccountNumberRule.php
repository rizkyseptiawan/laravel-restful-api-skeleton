<?php

namespace App\Rules;

use App\Models\AccountCategory;
use Illuminate\Contracts\Validation\Rule;

class AccountNumberRule implements Rule
{
    protected $errorMessage = '';

    protected $uniqueAccountNumberLength = 4;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($uniqueAccountNumberLength)
    {
        $this->uniqueAccountNumberLength = $uniqueAccountNumberLength;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!request()->has('account_category_id') && !request()->input('account_category_id')){
            $this->errorMessage = trans('Account category not present');
            return false;
        }
        
        $accountCategory = AccountCategory::find(request()->input('account_category_id'));
        
        if(is_null($accountCategory)){
            $this->errorMessage = trans('Account category not found');
            return false;
        }
        
        if(!str_contains($value, $accountCategory->account_category_number_prefix)){
            $this->errorMessage = trans('Invalid account number');
            return false;
        }
        
        $separatedAccountNumber = explode('-',$value);
        $uniqeAccountNumber = (string) $separatedAccountNumber[1];
        $countUniqeAccountNumberLength = strlen(substr($uniqeAccountNumber, 1));
        
        if($countUniqeAccountNumberLength != $this->uniqueAccountNumberLength){
            $this->errorMessage = trans('Account number must have '.  $this->uniqueAccountNumberLength .' digit');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Error :attribute {$this->errorMessage}";
    }
}
