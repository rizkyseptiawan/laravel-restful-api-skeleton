<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Rules\AccountNumberRule;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $accounts = Account::paginate($limit);
        return responseApi(trans('message.account.listed_success'), $accounts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'account_name' => ['required','string','unique:accounts,account_name'],
            'account_number' => ['required','string','unique:accounts,account_number', new AccountNumberRule(3)],
            'account_category_id' => ['required','exists:account_categories,id'],
            'account_description' => ['nullable','string'],
            'parent_account_id' => ['nullable','exists:accounts,id'],
            'is_active' => ['nullable','boolean'],
        ];
        $request->validate($rules);

        $createdAccount = Account::create([
            'account_name' => $request->input('account_name'),
            'account_number' => $request->input('account_number'),
            'account_description' => $request->input('account_description'),
            'account_category_id' => $request->input('account_category_id'),
            'parent_account_id' => $request->input('parent_account_id'),
            'is_active' => $request->has('is_active') ? $request->input('is_active', 1) : 1
        ]);

        return responseApi(trans('message.account.created_success'), $createdAccount);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = Account::findOrFail($id);
        return responseApi(trans('message.account.showed_success'), $account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account = Account::findOrFail($id);
        $rules = [
            'account_name' => ['required','string','unique:accounts,account_name,'. $account->id],
            'account_number' => ['required','string','unique:accounts,account_number,'. $account->id, new AccountNumberRule(3)],
            'account_category_id' => ['required','exists:account_categories,id'],
            'account_description' => ['nullable','string'],
            'parent_account_id' => ['nullable','exists:accounts,id'],
            'is_active' => ['nullable','boolean'],
        ];
        $request->validate($rules);

        $account->update([
            'account_name' => $request->input('account_name'),
            'account_number' => $request->input('account_number'),
            'account_description' => $request->input('account_description'),
            'account_category_id' => $request->input('account_category_id'),
            'parent_account_id' => $request->input('parent_account_id'),
            'is_active' => $request->has('is_active') ? $request->input('is_active', 1) : 1
        ]);

        return responseApi(trans('message.account.updated_success'), $account);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::findOrFail($id);
        if($account->is_locked){
            return responseApi(trans('message.account.deleted_fail_lock'), $account);
        }
        if($account->delete()){
            return responseApi(trans('message.account.deleted_success'));
        }

        return responseApi(trans('message.account.deleted_fail'));
    }
}
