<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AccountCategory;
use Illuminate\Http\Request;

class AccountCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $accountCategories = AccountCategory::query();
        if($request->input('type')){
            $accountCategories->where('account_category_type', $request->input('type'));
        }
        $accountCategories = $accountCategories->paginate($limit);
        return responseApi(trans('message.account_category.listed_success'), $accountCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'account_category_name' => ['required','unique:account_categories'],
            'account_category_type' => ['required', 'in:asset,expense,equity,liability,revenue']
        ];

        $request->validate($rules);

        $createdAccountCategory = AccountCategory::create([
            'account_category_name' => $request->input('account_category_name'),
            'account_category_type' => $request->input('account_category_type')
        ]);

        return responseApi(trans('message.account_category.created_success'), $createdAccountCategory);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accountCategory = AccountCategory::findOrFail($id);
        return responseApi(trans('message.account_category.showed_success'), $accountCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $accountCategory = AccountCategory::findOrFail($id);
        $rules = [
            'account_category_name' => ['required','unique:account_categories,account_category_name,'. $accountCategory->id],
            'account_category_type' => ['required', 'in:asset,expense,equity,liability,revenue']
        ];

        $request->validate($rules);

        $accountCategory->update([
            'account_category_name' => $request->input('account_category_name'),
            'account_category_type' => $request->input('account_category_type')
        ]);

        return responseApi(trans('message.account_category.updated_success'), $accountCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $accountCategory = AccountCategory::findOrFail($id);

        if($accountCategory->delete()){
            return responseApi(trans('message.account_category.deleted_success'));
        }
        
        return responseApi(trans('message.account_category.deleted_fail'));
    }
}
