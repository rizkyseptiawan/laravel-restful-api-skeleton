<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Business;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $businesses = Business::paginate($limit);
        return responseApi(trans('message.business.listed_success'), $businesses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'business_name' => ['required','unique:businesses,business_name'],
            'business_phone' => ['required','unique:businesses,business_phone'],
            'business_email' => ['required','unique:businesses,business_email'],
            'business_address' => ['nullable','string'],
            'business_description' => ['nullable','string'],
        ];

        $request->validate($rules);

        DB::beginTransaction();
        try {
            $dataToBeCreated = [
                'business_name' => $request->input('business_name'),
                'business_phone' => $request->input('business_phone'),
                'business_email' => $request->input('business_email'),
                'business_address' => $request->input('business_address'),
                'business_description' => $request->input('business_description')
            ];
            $createdBusiness = Business::create($dataToBeCreated);
            $createdBusiness->main_business_location()->create([
                'business_id' => $createdBusiness->id,
                'location_name' => $createdBusiness->business_name . ' Main Branch',
                'location_address' => $createdBusiness->business_address,
                'is_main_branch' => true
            ]);
            DB::commit();
    
            return responseApi(trans('message.business.created_success'), $createdBusiness);

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business = Business::findOrFail($id);
        return responseApi(trans('message.business.showed_success'), $business);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $business = Business::findOrFail($id);

        $rules = [
            'business_name' => ['required','unique:businesses,business_name'],
            'business_phone' => ['required','unique:businesses,business_phone'],
            'business_email' => ['required','unique:businesses,business_email'],
            'business_address' => ['nullable','string'],
            'business_description' => ['nullable','string'],
        ];

        $request->validate($rules);

        $dataToBeUpdated = [
            'business_name' => $request->input('business_name'),
            'business_phone' => $request->input('business_phone'),
            'business_email' => $request->input('business_email'),
            'business_address' => $request->input('business_address'),
            'business_description' => $request->input('business_description')
        ];
        $business->update($dataToBeUpdated);

        return responseApi(trans('message.business.updated_success'), $business);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $business = Business::findOrFail($id);
        if($business->delete()){
            return responseApi(trans('message.business.deleted_fail'));
        }
        return responseApi(trans('message.business.deleted_success'), $business);
    }
}
