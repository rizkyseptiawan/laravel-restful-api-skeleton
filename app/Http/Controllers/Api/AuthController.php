<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;
use DB;

class AuthController extends Controller
{
    /**
     * Unauthorized response
     *
     * @return void
     */
    public function unauthorized()
    {
        abort(401, trans('Invalid credentials. Please try logging in again.'));
    }

    /**
     * Login to get access token
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $rules = [
            'identity' => ['required'],
            'password' => ['required']
        ];
        $request->validate($rules);
        $identity = $request->input('identity');
        $identityType = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'non_email';

        if ($identityType == 'non_email') {
            $identityType = is_numeric($identity) ? 'phone' : 'username';
        }
        $authAttemptData = [
            $identityType => $request->identity,
            'password' => $request->password
        ];
        if (!Auth::attempt($authAttemptData)) {
            return responseApi('Invalid Credentials', [], 401, 2000);
        }
        $authUser = Auth::user();
        $createdToken = $authUser->createToken('LabdaCaraka PAT');
        $dataResponse = [
            'access_token' => $createdToken->accessToken,
            'roles' => $authUser->getRoleNames(),
            'token_type' => 'Bearer',
            'expires_at' => \Carbon\Carbon::parse($createdToken->token->expires_at)->toDateTimeString()
        ];
        $messageResponse = trans('Authentication successfully');
        return responseApi($messageResponse, $dataResponse);
    }

    /**
     * Register new user
     *
     * @param Request $request
     * @return void
     */
    public function register(Request $request)
    {
        $rules = [
            'firstname' => ['required','string'],
            'lastname' => ['nullable','string'],
            'username' => ['required','string','unique:users','min:5','alpha_dash'],
            'email' => ['required','email','unique:users'],
            'phone' => ['required','phone:AUTO','unique:users'],
            'password' => ['required','string','min:8','regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/','confirmed']
        ];
        $request->validate($rules);
        
        $newUserData = [
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'phone' => str_replace('+', '', $request->input('phone')),
                'password' => bcrypt($request->input('password')),
            ];
        $createdNewUser = User::create($newUserData);
        $createdNewUser->assignRole(['Customer']);

        return responseApi(trans('Registration was successful. Please login with your account.'));
    }

    /**
     * Logout from account
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $responseMessage = trans('Logout successfull');
        return responseApi($responseMessage);
    }

    /**
     * Request for getting password reset token
     *
     * @param Request $request
     * @return void
     */
    public function forgotPassword(Request $request)
    {
        $rules = [
            'email' => ['required','email','exists:users'],
        ];
        $request->validate($rules);

        $token = Str::random(64);
        $email = $request->input('email');
        $createdPasswordReset = DB::table('password_resets')->insert([
              'email' => $email,
              'token' => $token,
              'created_at' => Carbon::now()
        ]);

        $user = User::where('email', $email)->first();
        $resetPasswordLink = asset("password/reset/$token?email=$email");

        $mail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $mail->send('emails.reset_password_mail', ['user' => $user, 'resetLink' => $resetPasswordLink], function ($message) use ($user){
            $message->to($user->email, $user->firstname)
                    ->subject('Password Reset');
        });

        return responseApi('Password reset was sent');
        
    }

    /**
     * Request for changing password with given token
     *
     * @param Request $request
     * @return void
     */
    public function resetPassword(Request $request)
    {
        $rules = [
            'email' => ['required','email','exists:users'],
            'password' => ['required','string','min:8','regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/','confirmed'],
            'token' => ['required']
        ];
        $request->validate($rules);

        $resetPassword = DB::table('password_resets')
                        ->where('token', $request->input('token'))
                        ->where('email', $request->input('email'))
                        ->first();
        if(is_null($resetPassword)){
            abort(404, trans('Not found.'));
        }
        $tokenCreatedTime = Carbon::parse($resetPassword->created_at);
        $timeNow = Carbon::now();
        $tokenTimeDiffNow = $timeNow->diffInMinutes($tokenCreatedTime);
        $isTokenExpired = $tokenTimeDiffNow > 60 ? true : false;
        if($isTokenExpired){
            abort(401, trans('Reset password token was expired.'));
        }

        $userData = User::where('email', $request->input('email'))->first();
        $userData->update([
            'password' => bcrypt($request->input('password'))
        ]);

        return responseApi(trans('Password reset has been done successfully. Please go to login page.'));

    }

    public function authenticatedUser(Request $request)
    {
        $authUser = Auth::user();
        $roles = $authUser->getRoleNames();
        $permissions = $authUser->getPermissionsViaRoles()->pluck('name')->toArray();
        $privatePermissions = $authUser->getPermissionNames()->toArray();
        $allPermissions = array_merge($permissions, $privatePermissions);
        return responseApi(trans('Success'), [
            'personal' => $authUser,
            'roles' => $roles,
            'permissions' => $permissions,
            'private_permission' => $privatePermissions,
            'all_permissions' => $allPermissions
        ]);
    }
}
