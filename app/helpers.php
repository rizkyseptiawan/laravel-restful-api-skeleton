<?php

if (! function_exists('put_permanent_env')) {
    function put_permanent_env($key, $value)
    {
        $path = app()->environmentFilePath();

        $escapedOne = preg_quote('="'.env($key).'"', '/');
        $escapedTwo = preg_quote('='.env($key), '/');

        file_put_contents($path, preg_replace(
            "/^{$key}{$escapedOne}|^{$key}{$escapedTwo}/m",
            "$key=\"$value\"",
            file_get_contents($path)
        ));
    }
}

if (! function_exists('responseApi')) {
    /**
     * Useful for returning response value in json format
     *
     * @param string $message
     * @param array $data
     * @param integer $statusCode
     * @param integer $responseCode
     * @param array $additionalResponse
     * @return void
     */
    function responseApi($message, $data =[], $statusCode = 200, $responseCode = 1000, $additionalResponse = []){
        $status = true;
        if($statusCode >= 400){
            $status = false;
        }
        $responseStatusCode = $responseCode + $statusCode;
        $response = [
            'code' => $responseStatusCode,
            'status' => $status,
            'message' => $message
        ];
        if(!empty($data)){
            $response['data'] = $data;
        }
        if(!empty($additionalResponse)){
            $response = $response + $additionalResponse;
        }
        return response()->json($response, $statusCode);
    }
}
